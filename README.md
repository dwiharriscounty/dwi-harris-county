Andreea Ionescu is an established figure in the legal community with a wealth of insider knowledge of the legal system. She understands criminal prosecution inside and out and will use this knowledge to fight for your rights.
Andreea graduated from University of Miami, School of Law in 2013.

Address: 1221 Studewood Street, Houston, TX 77008, USA

Phone: 832-509-0222

Website: http://dwiharriscounty.com/
